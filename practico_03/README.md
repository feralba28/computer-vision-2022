# Práctico 3 - Propiedades de video

1. Considerando el siguiente programa:

    ```py
    #! /usr/bin/env python
    # -*- coding: utf-8 -*-

    import sys, cv2

    if (len(sys.argv) > 1):
        filename = sys.argv[1]
    else:
        print('Pass a filename as first argument')
        sys.exit(0)

    cap = cv2.VideoCapture(filename)

    fourcc = cv2.VideoWriter_fourcc('X', 'V', 'I', 'D')
    framesize = (640, 480)
    out = cv2.VideoWriter('output.avi', fourcc, 20.0, framesize)

    delay = 33
    while(cap.isOpened()):
        ret, frame = cap.read()
        if ret is True:
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            out.write(gray)
            cv2.imshow('Image gray', gray)
            if cv2.waitKey(delay) & 0xFF == ord('q'):
                break
        else:
            break

    cap.release()
    out.release()
    cv2.destroyAllWindows()
    ```

    * ¿Cómo obtener el `frame-rate` o `fps` usando las OpenCV? Usarlo para no tener que *harcodear* el `delay` del `waitKey`.
    * ¿Cómo obtener el ancho y alto de las imágenes capturadas usando las OpenCV? Usarlo para no tener que *harcodear* el `frameSize` del video generado.

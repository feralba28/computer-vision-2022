# Práctico 2 - Segmentando una imagen

1. Escribir un programa en python que lea una imagen y realice un umbralizado binario, guardando el resultado en otra imagen.
    * NOTA: No usar ninguna función de las OpenCV, excepto para leer y guardar la imagen.
    * AYUDA: Se puede usar como base el siguiente template.

    ```py
    #! /usr/bin/env python
    # -*- coding: utf-8 -*-
    import cv2

    img = cv2.imread('hoja.png', 0)

    # Agregar código aquí
    # Para resolverlo podemos usar dos for anidados

    cv2.imwrite('resultado.png', img)
    ```

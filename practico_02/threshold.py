#! /usr/bin/env python
# -*- coding: utf-8 -*-

import cv2, numpy

def evaluate_pixel(pixel, threshold):
    return 255 if (pixel > threshold) else 0

def main():
    img = cv2.imread('hoja.png', cv2.IMREAD_GRAYSCALE)

    threshold = 192
    
    result = [[evaluate_pixel(pixel, threshold) for pixel in row] for row in img]
    
    cv2.imwrite('resultado.png', numpy.array(result))

if __name__ == '__main__':
    main()
import os, random

def adivina(cant_intentos):
    random_int = random.randint(0, 100)
    intento = 1

    print('¿Cuál es el número secreto?\n')

    while (intento <= cant_intentos):
        print('Ingrese un número entre 1 y 100:', end = ' ')
        entrada = input()

        while (not is_valid_input(entrada)):
            os.system('clear')
            print('La entrada ingresada no es válida.\nIngrese un número entre 1 y 100:', end = ' ')
            entrada = input()

        num = int(entrada)
        os.system('clear')

        if (num == random_int):
            print(f"\n¡Felicidades! Adivinaste el número secreto en el intento número {intento}.")
            return
        else:
            print('¡Muy alto!') if (num > random_int) else print('¡Muy bajo!')          
            print(f"Te quedan {cant_intentos - intento} intentos.")

        intento += 1
    else:
        os.system('clear')
        print(f"¡Game over! El número secreto era el {random_int}. Intenta nuevamente.\n")

def is_valid_input(input):
    if (input.isdigit()):
        return True if (int(input) >= 1 and int(input) <= 100) else False
    else:
        return False
